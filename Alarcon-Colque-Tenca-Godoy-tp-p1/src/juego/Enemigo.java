package juego;

import java.util.Random;

public class Enemigo {

	private Rectangulo cuerpo;
	private int velocidadX;
	private int velocidadY;
	private int ticks;
	private int direccion;
	private int anchoDisparo;
	private int altoDisparo;
	private int velocidadDisparo;
	private boolean trex;

	public Enemigo(int x, int y, int ancho, int alto, int velocidadX, int velocidadY, int direccion, int anchoDisparo, int altoDisparo, int velocidadDisparo)
	{
		this.cuerpo = new Rectangulo(x, y, ancho, alto);
		this.velocidadX = velocidadX;
		this.velocidadY = velocidadY;
		this.direccion = direccion;
		this.anchoDisparo = anchoDisparo;
		this.altoDisparo = altoDisparo;
		this.velocidadDisparo = velocidadDisparo;
		this.ticks = 0;
	}

	public boolean isTrex()
	{
		return trex;
	}

	public void setTrex(boolean trex)
	{
		this.trex = trex;
	}

	public void setDireccion(int direccion)
	{
		this.direccion = direccion;
	}

	public int getDireccion()
	{
		return direccion;
	}

	public void avanzar(int direccion)
	{
		if (this.direccion == 0)
			this.cuerpo.trasladar(-velocidadX, 0);
		else 
			this.cuerpo.trasladar(+velocidadX, 0);
	}

	public void setTicks(int ticks)
	{
		this.ticks = ticks;
	}

	public int contarTicks()
	{
		return this.ticks++;
	}

	public Rectangulo cuerpo()
	{
		return this.cuerpo;
	}

	public void caer()
	{
		this.cuerpo.trasladar(0, +velocidadY);
	}

	public void cambiarDireccionAleatoria()
	{
		Random random = new Random();
		int num = random.nextInt(2);

		this.setDireccion(num);
	}

	public boolean cercaDeColisionar(Rectangulo[] r1, boolean level2)
	{
		int y = !level2 ? 2 : 3;
		
		for (int i = 0; i < r1.length - 1; i++)
		{
			if(this.cuerpo.abajo() == r1[i].arriba() - y)
				return true;
		}

		return false;
	}
	
	public boolean colisionaConVigas(Rectangulo[] r2)
	{
		for (int i = 0; i < r2.length; i++) 
		{
			if (this.cuerpo.colisiona(r2[i]))
				return true;
		}
		return false;
	}

	public Disparo disparar() 
	{
		int velocidad = this.direccion == 1 ? this.velocidadDisparo : - this.velocidadDisparo;

		if (this.direccion == 1)
			return new Disparo(this.cuerpo.getX()+40, this.cuerpo.getY()-20, this.anchoDisparo, this.altoDisparo, velocidad, direccion);
		else
			return new Disparo(this.cuerpo.getX()-40, this.cuerpo.getY()-20, this.anchoDisparo, this.altoDisparo, velocidad, direccion);
	}
}
