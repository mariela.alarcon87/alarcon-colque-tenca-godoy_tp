package juego;

public class Disparo {

	private Rectangulo cuerpo;
	private int velocidadX;
	private int direccion;

	public Disparo(int x, int y, int ancho, int alto, int velocidadX, int direccion)
	{
		this.cuerpo = new Rectangulo(x, y, ancho, alto);
		this.velocidadX = velocidadX;
		this.direccion = direccion;
	}
	
	public int getDireccion()
	{
		return this.direccion;
	}

	public void avanzar()
	{
		this.cuerpo.trasladar(this.velocidadX, 0);
	}

	public Rectangulo cuerpo()
	{
		return this.cuerpo;
	}
}
