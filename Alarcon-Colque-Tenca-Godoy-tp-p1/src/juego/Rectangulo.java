package juego;

public class Rectangulo {

	private int x;
	private int y;
	private int ancho;
	private int alto;

	public Rectangulo(int x, int y, int ancho, int alto)
	{
		this.x = x;
		this.y = y;
		this.ancho = ancho;
		this.alto = alto;
	}

	public int getX()
	{
		return this.x;
	}

	public int getY()
	{
		return this.y;
	}

	public void setX(int x)
	{
		this.x = x;
	}

	public void setY(int y)
	{
		this.y = y;
	}

	public int getAncho()
	{
		return this.ancho;
	}

	public int getAlto()
	{
		return this.alto;
	}

	public void setAlto(int alto)
	{
		this.alto = alto;
	}

	public void trasladar(int dx, int dy)
	{
		this.x += dx;
		this.y += dy;
	}

	public int arriba()
	{
		return this.getY() - (this.getAlto()/2);
	}

	public int abajo()
	{
		return this.getY() + (this.getAlto()/2);
	}

	public int izquierda()
	{
		return this.getX() - (this.getAncho()/2);
	}

	public int derecha()
	{
		return this.getX() + (this.getAncho()/2);
	}

	public boolean colisiona(Rectangulo r)
	{
		boolean sinColisionArriba = this.arriba() > r.abajo();
		boolean sinColisionAbajo = this.abajo() < r.arriba();
		boolean sinColisionIzquierda = this.izquierda() > r.derecha();
		boolean sinColisionDerecha = this.derecha() < r.izquierda();

		return !(sinColisionArriba || sinColisionAbajo || sinColisionDerecha || sinColisionIzquierda);
	}

	public Rectangulo getBordeDerecho()
	{
		return new Rectangulo(803, this.getY(), 1, this.getAlto() - 2);
	}

	public Rectangulo getBordeIzquierdo()
	{
		return new Rectangulo(221, this.getY(), 1, this.getAlto() - 2);
	}

	public Rectangulo getHuecoDerecho()
	{
		return new Rectangulo(912, this.getY()+9, 224, 1);
	}

	public Rectangulo getHuecoIzquierdo()
	{
		return new Rectangulo(112, this.getY()+9, 224, 1);
	}
	
	public boolean colisionaConEntornoIzquierda()
	{
		return this.izquierda() <= 0;
	}

	public boolean colisionaConEntornoDerecha()
	{
		return this.derecha() >= 1024;
	}

	public boolean colisionaConEntornoArriba()
	{
		return this.arriba() < 0;
	}
}
