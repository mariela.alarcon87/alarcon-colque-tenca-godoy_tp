package juego;

import java.awt.Color;
import java.awt.Image;
import java.util.LinkedList;
import java.util.Random;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego
{
	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;

	// Variables y metodos propios de cada grupo
	Protagonista protagonista;
	Rectangulo[] vigas;
	Rectangulo commodore;
	Rectangulo corazon;
	LinkedList<Enemigo> enemigos;
	LinkedList<Disparo> disparosProtagonista;
	LinkedList<Disparo> disparosEnemigo;
	LinkedList<Disparo> aEliminarDisparos;
	LinkedList<Enemigo> aEliminarEnemigos;
	Random random;
	private boolean fin;
	private boolean nivel2;
	private String eliminados;
	private String puntaje;
	private String vida;
	private String nivel;
	private int numeroVida;
	private int numeroEliminados;
	private int contadorPuntaje;
	private int contadorNivel2;
	private int contadorEliminados;
	private int contadorHitTrex;
	private int ticksEnemigos;
	private int ticksRandom;
	private int multiplicadorNivel2;
	private int puntosNivel2;
	private Image castillo;
	private Image gameOver;
	private Image theEnd;
	private Image velociraptorDerecha;
	private Image velociraptorIzquierda;
	private Image barbariannaDerecha;
	private Image barbariannaIzquierda;
	private Image rayoDerecha;
	private Image rayoIzquierda;
	private Image fuegoDerecha;
	private Image fuegoIzquierda;
	private Image commodore128;
	private Image victoria;
	private Image follaje;
	private Image corazonVida;
	private Image caparazon;
	private Image nextLevel;

	Juego()
	{
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Barbarianna Viking Edition - Grupo 6 - v1", 1024, 768);

		// Inicializacion de protagonista
		this.protagonista = new Protagonista(60, 685, 30, 50, 5, 5, 10, 10, 10);
		
		//Inicializacion de commodore
		this.commodore = new Rectangulo(540, 90, 40, 40);

		//Inicializacion de corazon
		this.corazon = new Rectangulo(0, 0, 5 ,5);

		// Inicializacion de imagenes
		this.velociraptorDerecha = Herramientas.cargarImagen("imagenes/velociraptorDerecha.png");
		this.velociraptorIzquierda = Herramientas.cargarImagen("imagenes/velociraptorIzquierda.png");
		this.barbariannaDerecha = Herramientas.cargarImagen("imagenes/barbariannaDerecha.png");
		this.barbariannaIzquierda = Herramientas.cargarImagen("imagenes/barbariannaIzquierda.png");
		this.rayoDerecha = Herramientas.cargarImagen("imagenes/rayoDerecha.png");
		this.rayoIzquierda = Herramientas.cargarImagen("imagenes/rayoIzquierda.png");
		this.fuegoDerecha = Herramientas.cargarImagen("imagenes/fuegoDerecha.png");
		this.fuegoIzquierda = Herramientas.cargarImagen("imagenes/fuegoIzquierda.png");
		this.commodore128 = Herramientas.cargarImagen("imagenes/commodore128.png");
		this.castillo = Herramientas.cargarImagen("imagenes/castillo.png");
		this.victoria = Herramientas.cargarImagen("imagenes/victoria.jpg");
		this.gameOver = Herramientas.cargarImagen("imagenes/gameover.jpg");
		this.theEnd = Herramientas.cargarImagen("imagenes/end.jpg");
		this.follaje = Herramientas.cargarImagen("imagenes/follaje.png");
		this.corazonVida = Herramientas.cargarImagen("imagenes/corazon.png");
		this.caparazon = Herramientas.cargarImagen("imagenes/caparazon.png");
		this.nextLevel = Herramientas.cargarImagen("imagenes/nextLevel.jpg");

		// Inicializacion de texto
		this.eliminados = "Enemigos eliminados: ";
		this.puntaje = "Puntaje: ";
		this.vida = "Vidas: ";
		this.nivel = "Nivel: ";

		// Inicializacion de vidas
		this.numeroVida = 3;

		// Inicializacion de multiplicador para nivel2
		this.multiplicadorNivel2 = 1;

		// Inicializacion de puntos para nivel 2
		this.puntosNivel2 = 50;

		// Inicializacion de Random para corazones
		this.random = new Random();

		// Inicializacion de vigas
		this.vigas = new Rectangulo[5];
		this.vigas[0] = new Rectangulo(0, 720, 2400, 20);
		this.vigas[1] = new Rectangulo(0, 570, 1600, 20);
		this.vigas[2] = new Rectangulo(1024, 420, 1600, 20);
		this.vigas[3] = new Rectangulo(0, 270, 1600, 20);
		this.vigas[4] = new Rectangulo(1024, 120, 1600, 20);

		//Inicializacion de enemigos;
		this.enemigos = new LinkedList<Enemigo>();

		//Inicializacion de disparos
		this.disparosProtagonista = new LinkedList<Disparo>();
		this.disparosEnemigo = new LinkedList<Disparo>();

		//Inicializacion de listas a eliminar
		this.aEliminarDisparos = new LinkedList<Disparo>();
		this.aEliminarEnemigos = new LinkedList<Enemigo>();

		//Inicializacion de fin
		this.fin = false;

		// Inicia el juego!
		this.entorno.iniciar();
	}

	 /*
	 * Durante el juego, el método tick() será ejecutado en cada instante y 
	 * por lo tanto es el método más importante de esta clase. Aquí se debe 
	 * actualizar el estado interno del juego para simular el paso del tiempo 
	 * (ver el enunciado del TP para mayor detalle).
	 */

	public void tick()
	{
		if (this.protagonista.vivo()
			&& !victoria()
			&& this.contadorNivel2 < this.puntosNivel2 * this.multiplicadorNivel2)
		{
			dibujarEntidades();
			procesarEventos();
			actualizarEstados();
		}
		else
		{
			dibujarPantallas();

			if (this.entorno.sePresiono('y') && !this.fin)
				restart();

			if (this.entorno.estaPresionada('n') && !this.nivel2)
				this.fin = true;

			if (this.entorno.sePresiono('n') && this.nivel2)
			{
				this.multiplicadorNivel2++;
				this.contadorNivel2 = this.contadorPuntaje;
				this.nivel2 = false;
			}
		}
	}

	private void actualizarEstados()
	{
		if(this.protagonista.saltando())
			this.protagonista.avanzarSalto();

		for(Disparo disparo : this.disparosProtagonista)
			disparo.avanzar();

		this.protagonista.caer(vigas);

		this.protagonista.detener(vigas);

		this.ticksEnemigos++;
		if (this.ticksEnemigos == 120)
		{
			if(!this.nivel2)
				generarEnemigos(3, 3, 6, 10);
			else
				generarEnemigos(4, 4, 9, 5);

			this.ticksEnemigos = 0;
		}

		actualizarEnemigos();
		actualizarDisparosEnemigos();
		actualizarDisparosProtagonista();
		corazonAleatorio();

		for (Disparo disparo : this.aEliminarDisparos)
		{
			this.disparosEnemigo.remove(disparo);
			this.disparosProtagonista.remove(disparo);
		}

		for (Enemigo enemigo : this.aEliminarEnemigos)
			this.enemigos.remove(enemigo);
	}

	private void procesarEventos()
	{
		if(this.entorno.estaPresionada(this.entorno.TECLA_DERECHA)
		   && !this.protagonista.cuerpo().colisionaConEntornoDerecha()
		   && !this.protagonista.colisionaConVigasBordesIzquierda(vigas)
		   && !this.protagonista.isAgachado())
		{
			this.protagonista.moverDerecha();
		}

		if(this.entorno.estaPresionada(this.entorno.TECLA_IZQUIERDA)
		   && !this.protagonista.cuerpo().colisionaConEntornoIzquierda()
		   && !this.protagonista.colisionaConVigasBordesDerecha(vigas)
		   && !this.protagonista.isAgachado())
		{
			this.protagonista.moverIzquierda();
		}

		if (this.protagonista.contarTicks() > 100)
		{
			if(this.entorno.sePresiono(this.entorno.TECLA_ESPACIO) && !this.protagonista.isAgachado())
			{
				this.disparosProtagonista.add(this.protagonista.disparar());
				this.protagonista.setTicks(0);
			}
		}

		if(!this.protagonista.saltando() && this.entorno.sePresiono(this.entorno.TECLA_ARRIBA))
			this.protagonista.comenzarSalto();

		if (this.entorno.sePresiono('u'))
			this.protagonista.subir(vigas);

		if (this.entorno.estaPresionada(this.entorno.TECLA_ABAJO) && !this.protagonista.saltando())
		{
			this.protagonista.setAgachado(true);
			this.protagonista.cuerpo().setAlto(10);
		}
		else
		{
			this.protagonista.setAgachado(false);
			this.protagonista.cuerpo().setAlto(50);
		}

		if (this.entorno.estaPresionada('c'))
		{
			for(Disparo disparo : this.disparosEnemigo)
			{
				if (disparo.cuerpo().colisiona(this.protagonista.cuerpo()))
					this.aEliminarDisparos.add(disparo);
			}
		}
	}

	private void dibujarEntidades()
	{
		dibujarFondo();
		dibujarVigas();
		dibujarProtagonista();
		dibujarCommodore();
		dibujarEnemigos();
		dibujarDisparoProtagonista();
		dibujarDisparoEnemigos();
		dibujarEliminados();
		dibujarPuntaje();
		dibujarVida();
		dibujarFollaje();
		dibujarCorazon();
		dibujarCaparazon();
		dibujarNivel();
	}

	private void actualizarDisparosEnemigos()
	{
		for (Disparo disparo : this.disparosEnemigo)
		{
			disparo.avanzar();

			if (disparo.cuerpo().colisionaConEntornoDerecha() || disparo.cuerpo().colisionaConEntornoIzquierda())
				aEliminarDisparos.add(disparo);

			if (disparo.cuerpo().colisiona(this.protagonista.cuerpo()) && !this.entorno.estaPresionada('c'))
			{
				this.numeroVida--;
				this.aEliminarDisparos.add(disparo);

				if (this.numeroVida <= 0)
					this.protagonista.morir();
			}
		}

		for (Enemigo enemigo : this.enemigos)
		{
			if (enemigo.colisionaConVigas(this.vigas))
			{
				if (enemigo.contarTicks() == 260 && !enemigo.isTrex())
				{
					this.disparosEnemigo.add(enemigo.disparar());
					enemigo.setTicks(0);
				}
			}
		}
	}

	private void actualizarDisparosProtagonista()
	{
		for (Enemigo enemigo : this.enemigos) 
		{
			for (Disparo disparo : this.disparosProtagonista)
			{
				if (disparo.cuerpo().colisionaConEntornoDerecha() || disparo.cuerpo().colisionaConEntornoIzquierda())
					this.aEliminarDisparos.add(disparo);

				if (disparo.cuerpo().colisiona(enemigo.cuerpo()) && enemigo.isTrex())
				{
					this.contadorHitTrex++;
					if(contadorHitTrex == 3)
					{
						this.contadorPuntaje += 10;
						this.numeroEliminados++;
						this.contadorHitTrex = 0;
						this.aEliminarEnemigos.add(enemigo);

						if (!this.nivel2)
							this.contadorNivel2 += 10;
					}

					this.aEliminarDisparos.add(disparo);
				}

				if (disparo.cuerpo().colisiona(enemigo.cuerpo()) && !enemigo.isTrex())
				{
					this.contadorPuntaje += 3;
					this.contadorEliminados++;
					this.numeroEliminados++;
					this.aEliminarEnemigos.add(enemigo);

					if (!this.nivel2)
						this.contadorNivel2 += 3;

					this.aEliminarDisparos.add(disparo);
				}
			}
		}
	}

	private void actualizarEnemigos()
	{
		for (Enemigo enemigo : this.enemigos) 
		{
			if (!enemigo.colisionaConVigas(this.vigas)) 
				enemigo.caer();

			if (enemigo.colisionaConVigas(this.vigas))
				enemigo.avanzar(enemigo.getDireccion());

			if (enemigo.cuerpo().colisionaConEntornoIzquierda())
				enemigo.setDireccion(1);

			if (enemigo.cuerpo().colisionaConEntornoDerecha())
				enemigo.setDireccion(0);

			if (enemigo.cuerpo().colisionaConEntornoIzquierda() && enemigo.cuerpo().colisiona(this.vigas[0]))
			{
				enemigo.cuerpo().setX(880);
				enemigo.cuerpo().setY(0);
			}

			if (enemigo.cercaDeColisionar(this.vigas, this.nivel2))
				enemigo.cambiarDireccionAleatoria();

			if (enemigo.cuerpo().colisiona(this.protagonista.cuerpo()))
			{
				this.numeroVida--;
				this.aEliminarEnemigos.add(enemigo);

				if (this.numeroVida <= 0)
					this.protagonista.morir();
			}

			if (enemigo.cuerpo().colisiona(this.protagonista.cuerpo()) && enemigo.isTrex())
				this.protagonista.morir();
		}
	}

	private boolean victoria()
	{
		if(this.protagonista.cuerpo().colisiona(this.commodore))
			return true;

		return false;
	}

	private void generarEnemigos(int velocidadX, int velocidadY, int velocidadDisparo, int eliminadosParaTrex)
	{
		if (this.enemigos.size() <= 4)
			this.enemigos.add(new Enemigo(950, 25, 30, 40, velocidadX, velocidadY, 0, 30, 10, velocidadDisparo));

		if (this.contadorEliminados == eliminadosParaTrex)
		{
			Enemigo trex = new Enemigo(950, 25, 150, 80, 2, 3, 0, 30, 10, 5);
			trex.setTrex(true);
			this.contadorEliminados = 0;
			this.enemigos.add(trex);
		}
	}

	private void restart()
	{
		if (this.nivel2 && !this.protagonista.vivo() || victoria())
			this.nivel2 = false;

		this.protagonista.cuerpo().setX(60);
		this.protagonista.cuerpo().setY(685);
		this.protagonista.vivir(true);
		this.contadorPuntaje = 0;
		this.numeroEliminados = 0;
		this.contadorEliminados = 0;
		this.contadorHitTrex = 0;
		this.ticksEnemigos = 0;
		this.ticksRandom = 0;
		this.contadorNivel2 = 0;
		this.multiplicadorNivel2 = 1;
		this.numeroVida = 3;
		this.corazon.setX(0);
		this.corazon.setY(0);
		this.enemigos.clear();
		this.disparosEnemigo.clear();
		this.disparosProtagonista.clear();
	}

	private void corazonAleatorio()
	{
		this.ticksRandom++;

		if (this.ticksRandom == 500)
		{
			switch (random.nextInt(7))
			{
			case 0:
				this.corazon.setX(910);
				this.corazon.setY(700);
				this.ticksRandom = 250;
				break;
			case 1:
				this.corazon.setX(80);
				this.corazon.setY(550);
				this.ticksRandom = 250;
				break;
			case 2:
				this.corazon.setX(600);
				this.corazon.setY(550);
				this.ticksRandom = 250;
				break;
			case 3:
				this.corazon.setX(410);
				this.corazon.setY(400);
				this.ticksRandom = 250;
				break;
			case 4:
				this.corazon.setX(650);
				this.corazon.setY(250);
				this.ticksRandom = 250;
				break;
			case 5:
				this.corazon.setX(250);
				this.corazon.setY(700);
				this.ticksRandom = 250;
				break;
			case 6:
				this.corazon.setX(120);
				this.corazon.setY(250);
				this.ticksRandom = 250;
				break;
			default:
				break;
			}
		}

		if (this.protagonista.cuerpo().colisiona(corazon))
		{
			this.numeroVida++;
			this.contadorPuntaje++;
			this.corazon.setX(0);
			this.corazon.setY(0);
			this.ticksRandom = 0;

			if (!this.nivel2)
				this.contadorNivel2 += 1;
		}
	}

	private void dibujarDisparoProtagonista()
	{
		for(Disparo disparo : this.disparosProtagonista)
		{
			if (disparo.getDireccion() == 1)
				this.entorno.dibujarImagen(this.rayoDerecha, disparo.cuerpo().getX(), disparo.cuerpo().getY(), 0, 0.02);
			else
				this.entorno.dibujarImagen(this.rayoIzquierda, disparo.cuerpo().getX(), disparo.cuerpo().getY(), 270, 0.02);
		}
	}

	private void dibujarDisparoEnemigos()
	{
		for (Disparo disparo : this.disparosEnemigo)
		{
			if (disparo.getDireccion() == 1)
				this.entorno.dibujarImagen(this.fuegoDerecha, disparo.cuerpo().getX()-23, disparo.cuerpo().getY(), 0, 0.1);
			else
				this.entorno.dibujarImagen(this.fuegoIzquierda, disparo.cuerpo().getX()+23, disparo.cuerpo().getY(), 0, 0.1);
		}
	}

	private void dibujarProtagonista()
	{
		if (this.protagonista.vivo() && !this.protagonista.isAgachado())
		{
			if (this.protagonista.getDireccion() == 1)
				this.entorno.dibujarImagen(this.barbariannaDerecha, this.protagonista.cuerpo().getX(), this.protagonista.cuerpo().getY()-17, 0, 0.1);
			else
				this.entorno.dibujarImagen(this.barbariannaIzquierda, this.protagonista.cuerpo().getX(), this.protagonista.cuerpo().getY()-17, 0, 0.1);
		}
	}

	private void dibujarEnemigos()
	{
		for (Enemigo enemigo : this.enemigos)
		{
			if (enemigo.isTrex())
			{
				dibujarTrex(enemigo);
			}
			else
			{
				if (enemigo.getDireccion() == 1)
					this.entorno.dibujarImagen(this.velociraptorDerecha, enemigo.cuerpo().getX(), enemigo.cuerpo().getY()-6, 0, 0.12);
				else
					this.entorno.dibujarImagen(this.velociraptorIzquierda, enemigo.cuerpo().getX(), enemigo.cuerpo().getY()-6, 0, 0.12);
			}
		}
	}

	private void dibujarTrex(Enemigo enemigo)
	{
		if (enemigo.getDireccion() == 1)
			this.entorno.dibujarImagen(this.velociraptorDerecha, enemigo.cuerpo().getX(), enemigo.cuerpo().getY()-20, 0, 0.27);
		else
			this.entorno.dibujarImagen(this.velociraptorIzquierda, enemigo.cuerpo().getX(), enemigo.cuerpo().getY()-20, 0, 0.27);
	}

	private void dibujarVigas()
	{
		for (int i = 0; i < this.vigas.length ; i++)
			this.entorno.dibujarRectangulo(this.vigas[i].getX(), this.vigas[i].getY(), this.vigas[i].getAncho(), this.vigas[i].getAlto(), 0, Color.gray);
	}

	private void dibujarFondo()
	{
		this.entorno.dibujarImagen(this.castillo, this.entorno.ancho()/2, this.entorno.alto()/2, 0, 1);
	}

	private void dibujarCommodore()
	{
		this.entorno.dibujarImagen(this.commodore128, this.commodore.getX(), this.commodore.getY(), 0, 0.15);
	}

	private void dibujarGameOver()
	{
		this.entorno.dibujarImagen(this.gameOver, this.entorno.ancho()/2, this.entorno.alto()/2, 0);
	}

	private void dibujarEnd()
	{
		this.entorno.dibujarImagen(this.theEnd, this.entorno.ancho()/2, this.entorno.alto()/2, 0);
	}

	private void dibujarVictoria()
	{
		this.entorno.dibujarImagen(this.victoria, this.entorno.ancho()/2, this.entorno.alto()/2, 0);
	}

	private void dibujarPantallas()
	{
		if(!this.fin && !this.protagonista.vivo())
			dibujarGameOver();

		if (victoria())
			dibujarVictoria();

		if (this.fin)
			dibujarEnd();

		if (!this.fin && this.protagonista.vivo() && !victoria())
		{
			this.nivel2 = true;
			dibujarNextLevel();
		}
	}

	private void dibujarPuntaje()
	{
		this.entorno.cambiarFont(this.puntaje, 20, Color.white);
		this.entorno.escribirTexto(this.puntaje + this.contadorPuntaje, 910, 755);
	}

	private void dibujarVida()
	{
		this.entorno.cambiarFont(this.vida, 20, Color.white);
		this.entorno.escribirTexto(this.vida + this.numeroVida, 10, 755);
	}

	private void dibujarEliminados()
	{
		this.entorno.cambiarFont(this.eliminados, 20, Color.white);
		this.entorno.escribirTexto(this.eliminados + this.numeroEliminados, 405, 755);
	}

	private void dibujarFollaje()
	{
		this.entorno.dibujarImagen(this.follaje, 930, 20, 0, 0.3);
	}

	private void dibujarCorazon()
	{
		if (this.corazon.getX() != 0 && this.corazon.getY() != 0)
			this.entorno.dibujarImagen(this.corazonVida, this.corazon.getX(), this.corazon.getY()-1, 0, 0.03);
	}

	private void dibujarCaparazon()
	{
		if (this.protagonista.isAgachado())
			this.entorno.dibujarImagen(this.caparazon, this.protagonista.cuerpo().getX(), this.protagonista.cuerpo().getY()+10, 0, 0.18);
	}

	private void dibujarNextLevel()
	{
		this.entorno.dibujarImagen(this.nextLevel, this.entorno.ancho()/2, this.entorno.alto()/2, 0);
	}

	private void dibujarNivel()
	{
		this.entorno.cambiarFont(this.nivel, 20, Color.white);

		if (!this.nivel2)
			this.entorno.escribirTexto(this.nivel + 1, 10, 25);
		else
			this.entorno.escribirTexto(this.nivel + 2, 10, 25);
	}

	@SuppressWarnings("unused")
	public static void main(String[] args)
	{
		Juego juego = new Juego();
	}
}
