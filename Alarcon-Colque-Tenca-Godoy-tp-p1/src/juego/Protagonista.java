package juego;

public class Protagonista {

	private Rectangulo cuerpo;
	private int velocidadX;
	private int velocidadY;
	private int yAlturaMinima;
	private int yAlturaMaxima;
	private int anchoDisparo;
	private int altoDisparo;
	private int velocidadDisparo;
	private int direccion;
	private int ticks;
	private boolean subiendo;
	private boolean bajando;
	private boolean vivo;
	private boolean agachado;

	public Protagonista(int x, int y, int ancho, int alto, int velocidadX, int velocidadY, int anchoDisparo, int altoDisparo, int velocidadDisparo)
	{
		this.cuerpo = new Rectangulo(x, y, ancho, alto);
		this.velocidadX = velocidadX;
		this.velocidadY = velocidadY;
		this.yAlturaMinima = y;
		this.yAlturaMaxima = (int)(y - alto * 40);
		this.anchoDisparo = anchoDisparo;
		this.altoDisparo = altoDisparo;
		this.velocidadDisparo = velocidadDisparo;
		this.direccion = 1;
		this.vivo = true;
	}

	public Rectangulo cuerpo()
	{
		return this.cuerpo;
	}

	public int contarTicks()
	{
		return this.ticks++;
	}

	public void setTicks(int ticks)
	{
		this.ticks = ticks;
	}

	public int getDireccion()
	{
		return this.direccion;
	}
	
	public boolean isAgachado()
	{
		return this.agachado;
	}

	public void setAgachado(boolean agachado)
	{
		this.agachado = agachado;
	}

	public boolean vivo()
	{
		return this.vivo;
	}

	public void vivir(boolean vivo)
	{
		this.vivo = vivo;
	}

	public void morir()
	{
		this.vivo = false;
	}

	public void moverDerecha()
	{
		this.cuerpo.trasladar(velocidadX, 0);
		this.direccion = 1;
	}

	public void moverIzquierda()
	{
		this.cuerpo.trasladar(-velocidadX, 0);
		this.direccion = 0;
	}

	public boolean saltando()
	{
		return this.subiendo || this.bajando;
	}

	public void comenzarSalto()
	{
		this.subiendo = true;
	}

	public void setBajando(boolean bajando)
	{
		this.bajando = bajando;
	}

	public void setSubiendo(boolean subiendo)
	{
		this.subiendo = subiendo;
	}

	public void avanzarSalto()
	{
		if(this.subiendo)
			this.cuerpo.trasladar(0, -this.velocidadY);

		if(this.bajando)
			this.cuerpo.trasladar(0, this.velocidadY);

		if(this.cuerpo.getY() <= this.yAlturaMaxima)
		{
			this.bajando = true;
			this.subiendo = false;
		}

		if(this.cuerpo.getY() >= this.yAlturaMinima)
		{
			this.bajando = false;
		}
	}

	public void detener(Rectangulo[] r1)
	{
		int bordeVigasDerechasX = 814;
		int bordeVigasIzquierdasX = 212;

		for (int i = 1; i < r1.length; i++) 
		{
			if (i == 1 || i == 3)
			{
				if (this.cuerpo().abajo() == r1[i].arriba() && this.cuerpo().getX() < bordeVigasDerechasX)
				{
					this.setBajando(false);
					this.setSubiendo(false);
				}
			}
			else
			{
				if (this.cuerpo().abajo() == r1[i].arriba() && this.cuerpo().getX() > bordeVigasIzquierdasX)
				{
					this.setBajando(false);
					this.setSubiendo(false);
				}
			}
		}
	}

	public void caer(Rectangulo[] r1)
	{
		int bordeVigasDerechasX = 814;
		int bordeVigasIzquierdasX = 212;

		if (this.colisionaConVigas(r1) || this.colisionaConHuecosDerecha(r1) || this.colisionaConHuecosIzquierda(r1) || this.cuerpo.colisionaConEntornoArriba())
		{
			this.setBajando(true);
			this.setSubiendo(false);
		}
		
		for (int i = 1; i < r1.length; i++)
		{
			if (i == 1 || i == 3)
			{
				if (this.cuerpo.colisiona(r1[i]) && this.cuerpo.getX() > bordeVigasDerechasX)
				{
					this.setBajando(true);
					this.setSubiendo(false);
				}
			}
			else
			{
				if (this.cuerpo.colisiona(r1[i]) && this.cuerpo.getX() < bordeVigasIzquierdasX)
				{
					this.setBajando(true);
					this.setSubiendo(false);
				}
			}
		}
	}

	public void subir(Rectangulo[] r1)
	{
		int bordeVigasDerechasX = 814;
		int bordeVigasIzquierdasX = 212;
		
		if (this.cuerpo.colisiona(r1[0]) && this.cuerpo.getX() > bordeVigasDerechasX)
		{
			this.cuerpo.setX(r1[1].derecha()-50);
			this.cuerpo.setY(r1[1].arriba()-25);
			this.direccion = 0;
		}

		if (this.cuerpo.colisiona(r1[1]) && this.cuerpo.getX() < bordeVigasIzquierdasX)
		{
			this.cuerpo.setX(r1[2].izquierda()+50);
			this.cuerpo.setY(r1[2].arriba()-25);
			this.direccion = 1;
		}

		if (this.cuerpo.colisiona(r1[2]) && this.cuerpo.getX() > bordeVigasDerechasX)
		{
			this.cuerpo.setX(r1[3].derecha()-50);
			this.cuerpo.setY(r1[3].arriba()-25);
			this.direccion = 0;
		}

		if (this.cuerpo.colisiona(r1[3]) && this.cuerpo.getX() < bordeVigasIzquierdasX)
		{
			this.cuerpo.setX(r1[4].izquierda()+50);
			this.cuerpo.setY(r1[4].arriba()-25);
			this.direccion = 1;
		}
	}

	public boolean colisionaConVigas(Rectangulo[] r2)
	{
		for (int i = 1; i < r2.length; i++) 
		{
			if (this.cuerpo.colisiona(r2[i]))
				return true;
		}

		return false;
	}

	public boolean colisionaConVigasBordesDerecha(Rectangulo[] r1)
	{
		if (this.cuerpo.colisiona(r1[1].getBordeDerecho()) || this.cuerpo.colisiona(r1[3].getBordeDerecho()))
			return true;

		return false;
	}

	public boolean colisionaConVigasBordesIzquierda(Rectangulo[] r1)
	{
		if (this.cuerpo.colisiona(r1[2].getBordeIzquierdo()) || this.cuerpo.colisiona(r1[4].getBordeIzquierdo()))
			return true;

		return false;
	}

	public boolean colisionaConHuecosDerecha(Rectangulo[] r1)
	{
		if (this.cuerpo.colisiona(r1[1].getHuecoDerecho()) || this.cuerpo.colisiona(r1[3].getHuecoDerecho()))
			return true;

		return false;
	}

	public boolean colisionaConHuecosIzquierda(Rectangulo[] r1)
	{
		if (this.cuerpo.colisiona(r1[2].getHuecoIzquierdo()) || this.cuerpo.colisiona(r1[4].getHuecoIzquierdo()))
			return true;

		return false;
	}

	public Disparo disparar()
	{
		int velocidad = this.direccion == 1 ? this.velocidadDisparo : - this.velocidadDisparo;
		return new Disparo(this.cuerpo.getX(), this.cuerpo.getY(), this.anchoDisparo, this.altoDisparo, velocidad, direccion);
	}
}
